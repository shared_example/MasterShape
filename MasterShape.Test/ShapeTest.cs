﻿using FluentAssertions;
using Xunit;

namespace MasterShape.Test
{
    public class ShapeTest
    {
        [Fact]
        public async void ShapeTest_Circle_RadiusOverZero()
        {

            var circleCalc = new CircleCalculator();
            var circle = new CircleShape(20);
            var result = await circleCalc.Square(circle);
            result.Should().Be(1256.6370614359173);

        }

        [Fact]
        public async void ShapeTest_Triangle()
        {

            var triangleCalc = new TriangleCalculator();
            var triangle = new TriangleShape(20, 21, 29);
            var result = await triangleCalc.Square(triangle);
            result.Should().Be(210);

        }

        [Fact]
        public async void ShapeTest_IsRight_Triangle()
        {

            var triangleCalc = new TriangleCalculator();
            var triangle = new TriangleShape(20, 21, 29);
            var result = await triangleCalc.IsRightTriangle(triangle);
            result.Should().Be(true);

        }
    }
}