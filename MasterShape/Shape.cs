﻿namespace MasterShape
{
    public abstract class Shape<T> : IShape<T> where T : class
    {

        public virtual async Task<double> Square(T value)
        {
            throw new NotImplementedException();
        }

    }
}
