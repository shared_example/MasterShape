﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterShape
{
    public class TriangleCalculator : Shape<TriangleShape>
    {
        public TriangleCalculator() 
        { 
        
        }


        /// <summary>
        /// вычисление площади треугольника
        /// </summary>
        /// <param name="triangle"></param>
        /// <returns></returns>
        public sealed override async Task<double> Square(TriangleShape triangle)
        {

            // S=sqrt{p*(p-a)*(p-b)*(p-c)}
            var p = (triangle.SideA + triangle.SideB + triangle.SideC) / 2;
            var task = await Task.FromResult(Math.Sqrt(p * (p - triangle.SideA) * (p - triangle.SideB) * (p - triangle.SideC)));
            return task;
        }

        /// <summary>
        /// проверка, является ли треугольник прямоугольным
        /// </summary>
        /// <param name="triangle"></param>
        /// <returns></returns>
        public async Task<bool> IsRightTriangle(TriangleShape triangle)
        {
            double[] sides = new double[] { triangle.SideA, triangle.SideB, triangle.SideC };

            for (int i = 0; i < sides.Length - 1; i++)
            {
                for (int j = 0; j < sides.Length - i - 1; j++)
                {
                    if (sides[j + 1] < sides[j])
                    {
                        double swap = sides[j];
                        sides[j] = sides[j + 1];
                        sides[j + 1] = swap;
                    }
                }
            }

            return await Task.FromResult(Math.Pow(sides[0], 2) + Math.Pow(sides[1], 2) == Math.Pow(sides[2], 2));
        }
    }
}
