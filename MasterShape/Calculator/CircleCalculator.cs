﻿namespace MasterShape
{
    public class CircleCalculator : Shape<CircleShape>
    {
        public CircleCalculator() { }

        /// <summary>
        /// Вычисление площади круга
        /// </summary>
        /// <param name="circle"></param>
        /// <returns></returns>
        public sealed override async Task<double> Square(CircleShape circle)
        {
            return await Task.FromResult<double>(Math.PI * Math.Pow(circle.Radius, 2));
        }
    }
}
