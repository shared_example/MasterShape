﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterShape
{
    public class CircleShape
    {

        public CircleShape(double radius) 
        {
            if (radius < 0)
                throw new ArgumentException("Радиус не может быть меньше 0");

            Radius = radius;
        }

        public double Radius { get; private set; }
    }
}
