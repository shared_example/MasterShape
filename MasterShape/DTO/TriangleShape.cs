﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterShape
{
    public class TriangleShape
    {
        
        public TriangleShape(double sideA, double sideB, double sideC) 
        {
            if (sideA < 0 || sideB < 0 || sideC < 0)
                throw new ArgumentException("Стороны треугольника не могут быть меньше нуля");

            SideA = sideA;
            SideB = sideB;
            SideC = sideC;
        }

        public double SideA { get; private set; }

        public double SideB { get; private set; }

        public double SideC { get; private set; }

    }
}
