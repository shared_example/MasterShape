﻿namespace MasterShape
{
    public interface IShape<T>
    {
        Task<double> Square(T value);
    }
}