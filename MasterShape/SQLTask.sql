﻿
IF OBJECT_ID('tempdb..#product')            IS NOT NULL 
  DROP TABLE #product
IF OBJECT_ID('tempdb..#category')           IS NOT NULL 
  DROP TABLE #category
IF OBJECT_ID('tempdb..#product_categories') IS NOT NULL 
  DROP TABLE #product_categories

CREATE TABLE #product
(
  product_id INT,
  product_name VARCHAR(32)
)

CREATE TABLE #category
(
  category_id INT,
  category_name VARCHAR(32)
)

INSERT INTO #product (product_id, product_name)
VALUES  (1, 'product 1'),
        (2, 'product 2'),
        (3, 'product 3'),
        (4, 'product 4'),
        (5, 'product 5'),
        (6, 'product 6'),
        (7, 'product 7'),
        (8, 'product 8')
        

INSERT INTO #category (category_id, category_name)
VALUES  (1, 'category 1'),
        (2, 'category 2'),
        (3, 'category 3'),
        (4, 'category 4'),
        (5, 'category 5'),
        (6, 'category 6')        
        
        
        
CREATE TABLE #product_categories 
(
  product_id  INT FOREIGN KEY REFERENCES #product(product_id),
  category_id INT FOREIGN KEY REFERENCES #category(category_id),
  PRIMARY KEY (product_id, category_id)
)

INSERT INTO #product_categories
VALUES 
(1, 1),
(2, 3),
(3, 3),
(4, 2),
(4, 5),
(6, 1),
(6, 2),
(6, 4),
(7, 5)

SELECT 
  p.product_name, 
  c.category_name 
FROM #product p
  LEFT JOIN #product_categories pc  ON p.product_id   = pc.product_id
  LEFT JOIN #category           c   ON pc.category_id = C.category_id;       